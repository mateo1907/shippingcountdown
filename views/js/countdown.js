jQuery(document).ready(function($){
  var date = moment(shippingcountdown.currentDate +' '+ shippingcountdown.hour).format('YYYY-MM-DD HH:mm');
  var current = moment().format('YYYY-MM-DD HH:mm');
  var diff = moment(date).diff(current,'seconds')
  // console.log(date)
  if (diff <= 0) {
    date = moment(date).add(1,'days').format('YYYY-MM-DD HH:mm')
  }
  jQuery('#jcountdown').countdown(date, function(event) {
    $(this).html(event.strftime(''

      + '<span>%H</span> godz '
      + '<span>%M</span> min '
      + '<span>%S</span> sek'));
  });
  // console.log(date)


})
