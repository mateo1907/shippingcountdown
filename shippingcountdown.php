<?php
if (!defined('_PS_VERSION_')) {
  exit;
}

class ShippingCountdown extends Module
{
  public function __construct()
  {
    $this->name = 'shippingcountdown';
    $this->tab = 'front_office_features';
    $this->version = '1.0.0';
    $this->author = 'Mateusz Prasał';
    $this->need_instance = 0;
    $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    $this->bootstrap = true;

    parent::__construct();

    $this->displayName = $this->l('Time to Shipping - Countdown');


    $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');


  }

  public function install()
  {
    if (!parent::install() || !$this->registerHook('displayFooter') || !$this->registerHook('Header') ) {
      return false;
    }

    return true;
  }

  public function uninstall()
  {
    if (!parent::uninstall() ) {
      return false;
    }

    return true;
  }

  public function getContent()
  {

    return $this->customScripts() . $this->_postProcess() . $this->_displayForm();
  }

  public function _postProcess()
  {
    if (Tools::isSubmit('submit' . $this->name)) {
      Configuration::updateValue($this->name . '_hour', Tools::getValue($this->name . '_hour'));
      return $this->displayConfirmation($this->l('Wszystko w porządku'));
    }
  }

  public function _displayForm()
  {
    $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

    $helper = new HelperForm();

   // Module, token and currentIndex
   $helper->module = $this;
   $helper->name_controller = $this->name;
   $helper->token = Tools::getAdminTokenLite('AdminModules');
   $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

   // Language
   $helper->default_form_language = $default_lang;
   $helper->allow_employee_form_lang = $default_lang;

   // Title and toolbar
   $helper->title = $this->displayName;
   $helper->show_toolbar = true;        // false -> remove toolbar
   $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
   $helper->submit_action = 'submit'.$this->name;
   $helper->toolbar_btn = array(
       'save' =>
       array(
           'desc' => $this->l('Save'),
           'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
           '&token='.Tools::getAdminTokenLite('AdminModules'),
       ),
       'back' => array(
           'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
           'desc' => $this->l('Back to list')
       )
   );

   // Load current value
   $helper->fields_value[$this->name . '_hour'] = Configuration::get($this->name . '_hour');

   return $helper->generateForm($this->_renderForm());
  }

  public function _renderForm()
  {
    $fields_form[0]['form'] = array(
        'legend' => array(
            'title' => $this->l('Ustawienia'),
        ),
        'input' => array(
            array(
                'type' => 'text',
                'label' => $this->l('Godzina zakończenia wysyłki'),
                'name' => $this->name . '_hour',
                'size' => 20,
                'required' => true,
                'class' => $this->name . '_hour'
            )
        ),
        'submit' => array(
            'title' => $this->l('Save'),
            'class' => 'btn btn-default pull-right'
        )
    );

    return $fields_form;
  }

  public function hookdisplayFooter($params)
  {
    return $this->display(__FILE__,'view.tpl');
  }


  public function hookHeader($params)
  {
    Media::addJsDef([$this->name => [
      'hour' => Configuration::get($this->name .'_hour'),
      'currentDate' => date('Y-m-d')
      ]]);
    $this->context->controller->addJquery();
    $this->context->controller->addJS('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js');
    $this->context->controller->addJS('https://cdn.rawgit.com/hilios/jQuery.countdown/2.0.4/dist/jquery.countdown.min.js');
    $this->context->controller->addCSS($this->_path . 'views/css/countdown.css');
    $this->context->controller->addJS($this->_path . 'views/js/countdown.js');
  }


  public function customScripts()
  {
    $html = '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css">';
    $html .= '<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js" charset="utf-8"></script>';

    $html .= "
      <script>
        jQuery(document).ready(function($){
          $('#{$this->name}_hour').timepicker(
              { 'timeFormat': 'H:i' }
          );

        })
      </script>
    ";

    return $html;
  }
}
